# Freeling.io API server
### Project Structure

```
api-server-flask/
├── api/
│   ├── __init__.py
│   ├── config.py
│   ├── models/
│   │    ├── __init__.py
│   │    ├── users.py
│   │    ├── decks.py
│   │    └── words.py
│   └── routes/
├── migrations/
├── requirements.txt
└── app.py
```